// bash2fish_test
package main

import (
	"strconv"
	"testing"
)

func Test_rotr(t *testing.T) {
	tmp1, _ := strconv.ParseInt("0002111112", 3, 64)
	actual := strconv.FormatInt(int64(rotr(int(tmp1))), 3)
	expected := "2000211111"
	if actual != expected {
		t.Error("rotr function test failed")
	}
}

func Test_crz(t *testing.T) {
	tmp2, _ := strconv.ParseInt("0001112220", 3, 64)
	tmp3, _ := strconv.ParseInt("0120120120", 3, 64)
	actual := strconv.FormatInt(int64(crz(int(tmp2), int(tmp3))), 3)
	expected := "1001022211"
	if actual != expected {
		t.Error("crz function test failed")
	}
}
