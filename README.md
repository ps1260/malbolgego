# malbolgego

*The Go interpreter for the programming language from hell, [Malbogle](https://en.wikipedia.org/wiki/Malbolge).*

There is a lack of interpreters for Malbogle, not to mention lack of demand for them, and so I attempt to create a supply. Enjoy your venture into the 8th circle of programming hell, made possible by the Go programming language.