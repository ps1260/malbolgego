// malbolgego
package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

var enc_lut = []int{57, 109, 60, 46, 84, 86, 97, 99, 96, 117, 89, 42, 77, 75, 39, 88, 126, 120, 68,
	108, 125, 82, 69, 111, 107, 78, 58, 35, 63, 71, 34, 105, 64, 53, 122, 93, 38, 103,
	113, 116, 121, 102, 114, 36, 40, 119, 101, 52, 123, 87, 80, 41, 72, 45, 90, 110, 44,
	91, 37, 92, 51, 100, 76, 43, 81, 59, 62, 85, 33, 112, 74, 83, 55, 50, 70, 104,
	79, 65, 49, 67, 66, 54, 118, 94, 61, 73, 95, 48, 47, 56, 124, 106, 115, 98}
var crz_lut = [][]int{[]int{1, 0, 0}, []int{1, 0, 2}, []int{2, 2, 1}}

func main() {
	if len(os.Args) != 2 {
		fmt.Println("Please give a filename of a malbolge source file")
		return
	}
	lines := read_file(os.Args[1])
	replacer := strings.NewReplacer(" ", "", "\n", "", "\t", "")
	code := replacer.Replace(strings.Join(lines, ""))
	//fmt.Println(code)
	var memory [59049]int
	for i, b := range code {
		switch (i + int(b)) % 94 {
		case 4:
		case 5:
		case 23:
		case 39:
		case 40:
		case 62:
		case 68:
		case 81:
		default:
			panic("Error in malbolge code: [" + strconv.FormatInt(int64(i), 10) + "]=" + string(b))
		}
		memory[i] = int(b)
	}
	for i := len(code); i < 59049; i++ {
		memory[i] = crz(memory[i-2], memory[i-1])
	}
	a, c, d := 0, 0, 0
	for (c+memory[c])%94 != 81 {
		switch (c + memory[c]) % 94 {
		case 4:
			c = d
		case 5:
			fmt.Print(string(byte(a)))
		case 23:
			var b rune
			fmt.Scan("%c", &b)
			a = int(b)
		case 39:
			a = rotr(memory[d])
			memory[d] = a
		case 40:
			d = memory[d]
		case 62:
			a = crz(memory[d], a)
			memory[d] = a
		case 68:
		case 81:
		default:
		}
		c++
		d++
		memory[c-1] = enc(memory[c-1] % 94)
	}
	fmt.Println("\n\nEnd of execution")
	return
}

func pow3(in int) int {
	out := 1
	for i := 0; i < in; i++ {
		out *= 3
	}
	return out
}
func crz(m2, m1 int) int {
	out := 0
	in1, in2 := (m2/pow3(9))%3, (m1/pow3(9))%3
	for i := 8; i >= 0; i-- {
		out *= 3
		out += crz_lut[in1][in2]
		in1, in2 = (m2/pow3(i))%3, (m1/pow3(i))%3
	}
	out *= 3
	out += crz_lut[in1][in2]
	return out
}
func rotr(in int) int {
	return in/3 + (in%3)*pow3(9)
}
func enc(in int) int {
	return enc_lut[in]
}

func read_file(filename string) []string {
	file, err := os.Open(filename)
	if err != nil {
		panic("WARNING: Couldn't find file " + filename)
	}
	defer file.Close()
	var filecontents []string
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		filecontents = append(filecontents, scanner.Text())
	}
	return filecontents
}
